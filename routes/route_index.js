var express = require('express')

var main_route = require('./main_route.js')
// var test_route = require('./test_route.js')

module.exports = function(app){

  // static routes
  app.use('/public', express.static('./public'))

  // sections
  // app.get('/test', test_route.home)
  // app.get('/about', main_route.about)

  //app.get('/server', main_route.server)
  app.get('/server', main_route.home)

  // gets the data
  app.get('/monsters', function(req,res,next){

    var m = JSON.parse(JSON.stringify(the_model))

    the_model.length = 0
    res.send(m)

  })

  // creates the data
  app.post('/clicks/:x/:y', function(req,res,next){

    console.log(req.params)
    the_model.push({x:req.params.x, y:req.params.y})

    res.send('ok')
    //res.send(the_model)

  })

  // home
  app.get('/', main_route.mobile)

  // error handlers
  app.use(function(req,res,next){
    console.log('sending 404')
    res.status(404)
    res.render('404')
  })

  app.use(function(err,req,res,next){
    console.log(err.stack)
    res.status(500)
    res.render('500')
  })

}


var the_model = []
